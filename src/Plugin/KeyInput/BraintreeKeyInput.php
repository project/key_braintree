<?php

namespace Drupal\key_braintree\Plugin\KeyInput;

use Drupal\Component\Serialization\Json;
use Drupal\Core\Form\FormStateInterface;
use Drupal\key\Plugin\KeyInputBase;

/**
 * Defines a key input that provides a fields for a Dumbo connection.
 *
 * @KeyInput(
 *   id = "braintree",
 *   label = @Translation("Braintree API Credentials"),
 *   description = @Translation("A collection of fields to store Braintree Gateway API credentials.")
 * )
 */
class BraintreeKeyInput extends KeyInputBase {

  /**
   * {@inheritdoc}
   */
  public function defaultConfiguration() {
    return [
      'merchant' => '',
      'public' => '',
      'private' => '',
      'merchant_account' => '',
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function buildConfigurationForm(array $form, FormStateInterface $form_state) {
    $config = $this->getConfiguration();

    $key = $form_state->getFormObject()->getEntity();
    $definition = $key->getKeyType()->getPluginDefinition();
    $fields = $definition['multivalue']['fields'];

    foreach ($fields as $id => $field) {
      $form[$id] = [
        '#type' => 'textfield',
        '#title' => $field['label'],
        '#required' => $field['required'],
        '#maxlength' => 4096,
        '#default_value' => $config[$id],
        '#attributes' => ['autocomplete' => 'off'],
      ];
    }

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function processSubmittedKeyValue(FormStateInterface $form_state) {
    $values = $form_state->getValues();
    $processed_values = [
      'submitted' => $values,
      'processed_submitted' => $values,
    ];

    return $processed_values;
  }
}
