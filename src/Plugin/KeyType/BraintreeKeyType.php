<?php

namespace Drupal\key_braintree\Plugin\KeyType;

use Drupal\Component\Serialization\Json;
use Drupal\Core\Form\FormStateInterface;
use Drupal\key\Plugin\KeyType\AuthenticationMultivalueKeyType;

/**
 * Defines a key for Braintree credentials.
 *
 * @KeyType(
 *   id = "braintree_key",
 *   label = @Translation("Braintree"),
 *   description = @Translation("Braintree credentials"),
 *   group = "authentication",
 *   key_value = {
 *     "plugin" = "braintree"
 *   },
 *   multivalue = {
 *     "enabled" = true,
 *     "fields" = {
 *       "merchant" = {
 *         "label" = @Translation("Merchant ID"),
 *         "required" = true
 *       },
 *       "public" = {
 *         "label" = @Translation("Public Key"),
 *         "required" = true,
 *       },
 *       "private" = {
 *         "label" = @Translation("Private Key"),
 *         "required" = true,
 *       },
 *       "merchant_account" = {
 *         "label" = @Translation("Merchant Account Key"),
 *         "required" = false,
 *       },
 *     }
 *   }
 * )
 */

class BraintreeKeyType extends AuthenticationMultivalueKeyType {
  /**
   * {@inheritdoc}
   */
  public static function generateKeyValue(array $configuration) {
    return Json::encode($configuration);
  }

  /**
   * {@inheritdoc}
   */
  public function validateKeyValue(array $form, FormStateInterface $form_state, $key_value) {
    if (!is_array($key_value) || empty($key_value)) {
      return;
    }

    $input_fields = $form['settings']['input_section']['key_input_settings'];

    $definition = $this->getPluginDefinition();
    $fields = $definition['multivalue']['fields'];

    foreach ($fields as $id => $field) {
      if (!is_array($field)) {
        $field = ['label' => $field];
      }

      if (isset($field['required']) && $field['required'] === FALSE) {
        continue;
      }

      $error_element = isset($input_fields[ $id ]) ? $input_fields[ $id ] : $form;

      if (!isset($key_value[$id])) {
        $form_state->setError($error_element, $this->t('The key value is missing the field %field.', ['%field' => $id]));
      } elseif (empty($key_value[$id])) {
        $form_state->setError($error_element, $this->t('The key value field %field is empty.', ['%field' => $id]));
      }
    }
  }
}
