## INTRODUCTION

Key Braintree is an extension to the Key module and provides a new
key type and supporting inputs to easily add a key containing
credentials for the [Braintree Gateway Payment Provider](https://www.braintreegateway.com/).

## REQUIREMENTS

This module is tested only on Drupal 9.0 and above but is compatible and should work on Drupal 8.8

## INSTALLATION

Install key_braintree using a standard method for installing a contributed Drupal
module, either by downloading the package or using composer with `composer require drupal/key_braintree`

## USING A KEY

Follow the [Key](https://drupal.org/project/key) documentation for getting and using keys
and adding a `key_select` element to your form in order to select your key,
you can spice the select box by adding a filter to select only braintree keys:

```
$form['braintree_auth'] = [
  '#type' => 'key_select',
  '#title' => $this->t('Braintree Auth'),
  '#key_filters' = ['type' => 'braintree_key']
];
```
